# Keyboards

This repository holds VIA configuration files (and potentially custom QMK code) for my mechanical keyboards.

## Neo Ergo

**Product page**: <https://www.qwertykeys.com/products/neo-ergo> (mine is the tri-mode PCB version)

The keyboard doesn't have open-sourced firmware. It works on [VIA](https://usevia.app) using a custom [Draft Definitions file](https://cdn.shopify.com/s/files/1/0508/8716/4061/files/ergo-trimode.json). The stock's binary firmware and configuration are available at <https://www.qwertykeys.com/pages/fw>.

The Draft Definitions file is vendored into [this repo](./neo-ergo/ergo-trimode.json) for convenient usage.
